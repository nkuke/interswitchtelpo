var exec = require('cordova/exec');

modules.exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'InterswitchTelpo', 'coolMethod', [arg0]);
};


modules.exports.printText = function (arg0, success, error) {
    exec(success, error, 'InterswitchTelpo', 'printText', [arg0]);
};