package cordova.plugin.telpo;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class InterswitchTelpo extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("printText")) {
             
            this.printText(args, callbackContext);
            return true;
        }
        return false;
    }

 
    private void printText(JSONArray args, CallbackContext callback){

            if(args !=null){
                try{

                    int param1=Integer.parseInt(args.getJSONObject(0).getString("param1"));
                    int param2=Integer.parseInt(args.getJSONObject(0).getString("param1"));

                    int answer=param1+param2;
                    callback.success("The answer is "+answer);

                }
                catch(Exception ex){
                    callback.error("Failed to execute action");
                }

             }
            else{
                callback.error("Failed to execute action");
            }
    }


}
